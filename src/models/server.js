const express = require('express');
const cors = require('cors');

const { dbConnection } = require('../database/config.js');

module.exports = class Server {

    constructor() {
        this.app = express();
        this.port = process.env.PORT;

        //Paths
        this.userPath = '/api/user'
        this.geocodingPath = '/api/geocoding'

        // Conectar a base de datos
        this.conectarDB();

        // Middlewares
        this.middlewares();

        // Rutas de mi aplicación
        this.routes();
    }

    async conectarDB() {
        await dbConnection();
    }

    middlewares() {

        // CORS        
        this.app.use(cors());

        // Lectura y parseo del body
        this.app.use(express.json());

        this.app.use(express.text());

    }

    routes() {
        //Routes
        this.app.use(this.userPath, require('../modules/user/routes/user'));
        this.app.use(this.geocodingPath, require('../modules/geocoding/routes/geocoding'));
    }

    listen() {
        this.app.listen(this.port, () => {
            console.log('Servidor corriendo en puerto', this.port);
        });
    }

}
