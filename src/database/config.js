const mongoose = require('mongoose');

const dbConnection = async () => {
    try {
        await mongoose.connect(process.env.MONGODB_CNN, {
            useNewUrlParser: false,
            useUnifiedTopology: true,
        })

        console.log('Base de Datos Online');
    } catch (error) {
        throw new Error('Error al iniciar la base de datos');
    }
}

module.exports = { dbConnection };