const bcryptjs = require('bcryptjs');

const encryptPassword = (password = '') => {
    const salt = bcryptjs.genSaltSync();
    return bcryptjs.hashSync(password.trim(), salt);
}

const comparePassword = (password = '', storedPassword) => {
    return bcryptjs.compareSync(password.trim(), storedPassword);
}

module.exports = {
    encryptPassword,
    comparePassword
}