const jwt = require('jsonwebtoken');

const createToken = (user) => {
    return new Promise((resolve, reject) => {

        jwt.sign(user, process.env.SECRET, {
            expiresIn: "1h"
        }, (err, token) => {
            if (err) {
                reject();
                return;
            }
            resolve(token);
            return;
        });

    })
}

const createRefreshToken = (user) => {
    return new Promise((resolve, reject) => {

        jwt.sign(user, process.env.SECRET_REFRESH, {
            expiresIn: "24h"
        }, (err, token) => {
            if (err) {
                reject();
                return;
            }
            resolve(token);
            return;
        });

    })
}

const checkRefreshToken = (userJWT) => {

    if (!userJWT) {
        return res.status(401).json({
            errors: [{
                msg: 'User is not logged'
            }]
        })
    }

    try {
        const user = jwt.verify(userJWT, process.env.SECRET_REFRESH);
        return user;
    } catch (err) {
        return res.status(401).json({
            errors: [
                {
                    msg: "Token is not valid"
                }
            ]
        })
    }
}

module.exports = {
    createToken,
    createRefreshToken,
    checkRefreshToken
}