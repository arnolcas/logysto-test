const jwt = require('jsonwebtoken');
const { response, request } = require('express');
const res = require('express/lib/response');

const checkUser = (req = request, res = response, next) => {
    const userJWT = req.header('x-token');

    if (!userJWT) {
        return res.status(401).json({
            errors: [{
                msg: 'User is not logged'
            }]
        })
    }

    try {
        const { user } = jwt.verify(userJWT, process.env.SECRET);
        req.user = user;
        next();
    } catch (err) {
        return res.status(401).json({
            errors: [
                {
                    msg: "Token is not valid"
                }
            ]
        })
    }
}

module.exports = {
    checkUser
}