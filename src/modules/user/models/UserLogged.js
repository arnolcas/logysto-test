const { Schema, model } = require('mongoose');

const UserLoggedSchema = Schema({
    user: {
        type: Schema.Types.ObjectId,
        required: true,
    },
    token: {
        type: String,
        required: true,
    },
    refreshToken: {
        type: String,
        required: true,
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    },
},{
    collection: 'loggedUsers'
})

UserLoggedSchema.pre('update', function(next){
    this.updatedAt = Date.now();
    next();
});

module.exports = model('UserLogged', UserLoggedSchema);