const { Schema, model } = require('mongoose');

const UserSchema = Schema({
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    },
}, {
    collection: 'users',
});

UserSchema.methods.toJSON = function () {
    const { __v, password, createdAt, updatedAt, ...user } = this.toObject();
    return user;
}

UserSchema.pre('save', function(next){
    this.updatedAt = Date.now();
    next();
});

module.exports = model('User', UserSchema);