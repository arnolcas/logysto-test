const { Router } = require('express');
const { check } = require('express-validator');
const { fieldValidator } = require('../../../middlewares/validator');
const { registerUser, loginUser, refreshToken } = require('../controllers/auth.controller');


const router = Router();

router.post('/', [
    check('email').isEmail(),
    check('password').not().isEmpty(),
    fieldValidator
], registerUser);

router.post('/login', [
    check('email').isEmail(),
    check('password').not().isEmpty(),
    fieldValidator
], loginUser);

router.post('/refresh', [
    check('token').not().isEmpty(),
    fieldValidator
], refreshToken);

module.exports = router;