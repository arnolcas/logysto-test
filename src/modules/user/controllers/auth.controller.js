const { request, response } = require('express');
const User = require('../models/user');
const UserLogged = require('../models/UserLogged');
const { createToken, createRefreshToken, checkRefreshToken } = require('../repository/jwt');
const { encryptPassword, comparePassword } = require('../repository/password');

const registerUser = async (req = request, res = response) => {
    const { email, password } = req.body;

    const encryptedPassword = encryptPassword(password);

    const user = new User({
        email,
        password: encryptedPassword,
    })

    await user.save();

    return res.json({
        msg: "Usuario registrado",
        user
    })
}

const loginUser = async (req = request, res = response) => {
    const { email, password } = req.body;

    const user = await User.findOne({
        email
    });

    if (!user) {
        return res.status(401).json({
            errors: [{
                msg: 'Usuario no encontrado'
            }]
        })
    }

    const checkPassword = comparePassword(password, user.password);
    if (!checkPassword) {
        return res.status(401).json({
            errors: [{
                msg: 'Password invalido'
            }]
        })
    }

    const token = await createToken(user.toJSON());
    const refreshToken = await createRefreshToken(user.toJSON());

    // const loginLogger = new UserLogged({
    //     user,
    //     token,
    //     refreshToken,
    // });

    await UserLogged.updateOne({
        _id: user._id.toString()
    }, {
        token,
        refreshToken,
    }, {
        upsert: true,
        setDefaultsOnInsert: true
    })
    // await loginLogger.save();

    return res.json({
        user,
        token,
        refreshToken
    })

}



const refreshToken = async (req = request, res = response) => {
    const refreshToken = req.body.token;

    const { iat, exp, ...user } = checkRefreshToken(refreshToken);

    if (!user) {
        res.status(401).json({ errors: [{ msg: "Token is not valid" }] })
    }

    const token = await createToken(user);

    await UserLogged.updateOne({
        _id: user._id
    }, {
        token,
        refreshToken,
    }, {
        upsert: true,
        setDefaultsOnInsert: true
    })
    // await loginLogger.save();

    return res.json({
        user,
        token,
        refreshToken
    })

}

module.exports = {
    registerUser,
    loginUser,
    refreshToken
}