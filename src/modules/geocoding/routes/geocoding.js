const { Router } = require('express');
const { query } = require('express-validator');
const {checkUser} = require('../../user/middlewares/auth');
const { fieldValidator } = require('../../../middlewares/validator');
const { getCordinates } = require('../controllers/geocoding.controller');

const router = Router();

router.get('/', [
    checkUser,
    query('address').not().isEmpty(),
    query('city').not().isEmpty(),
    query('country').not().isEmpty(),
    fieldValidator
], getCordinates);


module.exports = router;