const Cordinates = require("./Cordinates");
const axios = require('axios').default

module.exports = class Google extends Cordinates {

    constructor() {
        super();
        this.baseUrl = 'https://maps.googleapis.com/maps/api/geocode/json';
        this.apiKey = process.env.GOOGLE_APIKEY;
    }

    geocoding = (address, city, country) => {
        return new Promise(async (resolve, reject) => {
            const response = await axios.get(`${this.baseUrl}?address=${address} ${city} ${country}&key=${this.apiKey}`, { timeout: 5000 });
            if(response.data == undefined){
                reject()
                return;
            }
            const result = response.data.results;
            if(!result.length){
                reject();
                return;
            }            
            resolve(result[0].geometry.location);
        });
    }
}