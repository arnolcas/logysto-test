const Google = require("./Google");
const Here = require("./Here");
const Tomtom = require("./Tomtom");
const Mapbox = require("./Mapbox");

module.exports = class Geocoding {
    repository = [];

    constructor() {
        this.repository.push(new Google());
        this.repository.push(new Here());
        this.repository.push(new Tomtom());
        this.repository.push(new Mapbox());
    }

    getGeocodingPromise(address, city, country) {
        const returns = [];
        for (const r in this.repository) {
            returns.push(this.repository[r].geocoding(address, city, country));
        }

        return returns;
    }
}