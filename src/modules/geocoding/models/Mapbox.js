const Cordinates = require("./Cordinates");
const axios = require('axios').default

module.exports = class Google extends Cordinates {

    constructor() {
        super();
        this.baseUrl = 'https://api.mapbox.com/geocoding/v5/mapbox.places';
        this.apiKey = process.env.MAPBOX_APIKEY;
    }

    geocoding = (address, city, country) => {
        return new Promise(async (resolve, reject) => {
            reject();
            return;
            const response = await axios.get(`${this.baseUrl}/${address} ${city} ${country}.json?limit=5&access_token=${this.apiKey}`, { timeout: 5000 });
            if (response.data == undefined) {
                reject()
                return;
            }
            const result = response.data.features;
            if (!result.length) {
                reject();
                return;
            }


            resolve({ lat: result[0].geometry.coordinates[1], lon: result[0].geometry.coordinates[0] });
        });
    }
}