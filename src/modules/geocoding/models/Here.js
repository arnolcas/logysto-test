const Cordinates = require("./Cordinates");
const axios = require('axios').default

module.exports = class Here extends Cordinates {

    constructor() {
        super();
        this.baseUrl = 'https://geocode.search.hereapi.com/v1';
        this.apiKey = process.env.HERE_APIKEY;
    }

    geocoding = (address, city, country) => {
        return new Promise(async (resolve, reject) => {
            reject();
            return;
            const response = await axios.get(`${this.baseUrl}/geocode?q=${address} ${city} ${country}&apikey=${this.apiKey}`, { timeout: 5000 });
            if(response.data == undefined){
                reject()
                return;
            }
            const result = response.data.items;
            if(!result.length){
                reject();
                return;
            }            
            resolve(result[0].position);
        });
    }
}