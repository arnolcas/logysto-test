const Cordinates = require("./Cordinates");
const axios = require('axios').default

module.exports = class Tomtom extends Cordinates {

    constructor() {
        super();
        this.baseUrl = 'https://api.tomtom.com/search/2';
        this.apiKey = process.env.TOMTOM_APIKEY;
    }

    geocoding = (address, city, country) => {
        return new Promise(async (resolve, reject) => {
            reject();
            return;
            const response = await axios.get(`${this.baseUrl}/search/${address} ${city} ${country}.json?key=${this.apiKey}`, { timeout: 5000 });
            if(response.data == undefined){
                reject()
                return;
            }
            const result = response.data.results;
            if(!result.length){
                reject();
                return;
            }            
            resolve(result[0].position);
        });
    }
}