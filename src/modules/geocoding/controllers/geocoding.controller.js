const { request, response } = require('express');
const Geocoding = require('../models/Geocoding');

const getCordinates = async (req = request, res = respose) => {
    const { address, city, country } = req.query;

    const geocoding = new Geocoding();
    const cordinates = await Promise.allSettled(geocoding.getGeocodingPromise(address, city, country));

    if (!cordinates.length) {
        res.status(404).json({
            errors: [
                {
                    msg: "No se ha encontrado esta direccion",
                },
            ]
        })
    }

    for (const i in cordinates) {
        if (cordinates[i].status === "fulfilled") {
            return res.json({
                cordinates: cordinates[i].value
            });
        }
    }

    return res.status(404).json({
        errors: [
            {
                msg: "No se ha encontrado esta direccion",
            },
        ]
    })
}

module.exports = {
    getCordinates,
}