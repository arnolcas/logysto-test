const { validationResult } = require('express-validator');


const fieldValidator = (req, res, next) => {

    const errors = validationResult(req); //Cargamos todos los errores validados en los checks

    if (!errors.isEmpty()) { // comprobamos que no existen errores
        return res.status(400).json(errors); //Enviamos los errores con un status 400
    }

    next(); //Continuamos con el siguiente middleware
}

module.exports = { fieldValidator }